func reverseArray(arr: [Int]) -> [Int] {
	var revArr = [Int]()
	for item in arr {
		revArr.insert(item, at: 0)
	}
	return revArr
}
